"""
using Python to set values on M8190A and read EVM from running copy of 89601B sofwtare

with major help from Kevin Klug
Author: Gennady Farber

klug@google.com
"""

from datetime import datetime
#import matplotlib.pyplot as plt
import numpy as np
#import sys
import time
#import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\MindspeedScripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6', cmd_delay=0.1)

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

chip = 'A1'; # this is number on the board
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
AMPL = np.linspace(-10,-3,8)
N=16;
N2=32
Fosc = np.zeros(N)
Power = np.zeros(N)
#Pout=np.zeros(len(AMPL))
#ACP_Ref=np.zeros(len(AMPL))
#ACP_Lo=np.zeros(len(AMPL))
#ACP_Up=np.zeros(len(AMPL))
## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.send_file('C:\R2SiliconTest\CSR\\alogic_synOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\\syn_On.spi')
# measure VCO Osc Freq
regVal = hex2dec('0x000482A1')
regHexW2 = '0x00000041'
regHex = dec2hex(regVal)
### LSB Sweep, 4bit
mono.send_cmd('iwl 0x0002B004 ' + regHexW2)
for x in range(0,15): 
    print 'LSB Sweep ' + str(x)
    regVal = regVal + 2**19 * x;
    regHex = dec2hex(regVal)
    mono.send_cmd('iwl 0x0002B000 ' + regHex)
    [Foscl[x], Powerl[x]] = kt.run_sequence('SA_freq')
    time.sleep(2)

#### NSB Sweep,5 bit
regVal = hex2dec('0x000482A1')
regHex = dec2hex(regVal)
mono.send_cmd('iwl 0x0002B004 ' + regHexW2)
for x in range(0,31): 
    print 'NSB Sweep ' + str(x)
    regVal = regVal + 2**23 * x;
    regHex = dec2hex(regVal)
    mono.send_cmd('iwl 0x0002B000 ' + regHex)
    [Foscn[x], Powern[x]] = kt.run_sequence('SA_freq')
    time.sleep(2)

#### MSB Sweep,5 bit
regVal = hex2dec('0x000482A1')
regValW2 = hex2dec('0x00000040')
regHex = dec2hex(regVal)
for m in range(0,1):
	for x in range(0,15): 
		print 'NSB Sweep ' + str(x)
		regVal = regVal + 2**28 * x;
		regHex = dec2hex(regVal)
		regValW2 = regVal + m;
		regHexW2 = dec2hex(regValW2)
		mono.send_cmd('iwl 0x0002B000 ' + regHex)
		mono.send_cmd('iwl 0x0002B004 ' + regHexW2)
		time.sleep(2)
		[Foscm[x], Powerm[x]] = kt.run_sequence('SA_freq')

i_x= np.linspace(0,N-1,N);
i_x2= np.linspace(0,N2-1,N2);
fileName = chip + '_vco_freq_sweep_msb_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, np.transpose([i_x,Foscl]), fmt='%.3e', delimiter='\t',header='Freq vs LSB')
fileName = chip + '_vco_freq_sweep_msb_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, np.transpose([i_x2,Foscn]), fmt='%.3e', delimiter='\t',header='Freq vs NSB')
fileName = chip + '_vco_freq_sweep_msb_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, np.transpose([i_x2,Foscm]), fmt='%.3e', delimiter='\t',header='Freq vs MSB')
# bring chip to default
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
mono.close()

