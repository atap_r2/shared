#!/usr/bin/python
'''
% Sine fitting the samples; fit input to.
% Can also handle non-evenly sampled data.
% Don't assume samples contain N*cycle of sine wave.
% y = os + Ain*sin(wt+p) , where -pi < p < pi
% function [ d, outHarm ] = effbits( data, fin, fs, modulo, Nh, isplot, detail)
%
% OUTPUTS:
% d       : Contains parameters ENOB, SFDR, SNDR, SNR
% outHarm : Harmonics
%
% INPUTS:
% data  : single or two column data
%		  For two column data: first column is time, 2nd column is data
%		  If more than two column, column 3+ are ignored.
%
% fin   : signal freq, currently doesn't calibrate freq for now
%
% fs    : fs is not needed if the first column contains time
%
% modulo: 1 plot the modulo result (new_time=mod(time,1/fin))
%         0 plot with the original x-axis
%
% Nh    : number harmonic to fit, try 5
%
% isplot: 1 plot the fitting results, err, harmonics etc
%         0 if you don't want any plot.
%
% detail: 1 output original waveform, fitting sine, err as output
%
% NOTE:
% Compare to unix version of effbits in SCS.
% ENOB, THD, HDs are exactly the same because both use Least Square Method for fitting
% jitter: is a few % off. 
% 	Assumption: After remove fundamental and harmonics, the
% 	residue noise is jitter noise and thermal noise.
% 	I sum up the jitter on from (pi/4+3pi/4) and jitter on (3*pi/4+5*pi/4).
%	From the difference we find calculate the jitter.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% examples
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% e.g. 1: Results on the screen, plot reconstructed waveform etc.
%
%    effbits( data, fin, fs, 1, 7, 1, 0);
%    print out SNR,THD,HDs ect. on screen
%
% e.g. 2: Results on the screen, plot reconstructed waveform etc, 
%       and results assigned to variables
%
%    [dx harmx] = effbits( data, fin, fs, 1, 7, 1, 0);
%    output parameters are assigned to struct "dx".
%    To access data: eg. SNR is in dx.snr
%                        ENOB is in dx.enob
%                        type dx will give all available results.
%                        harmx contains HDs details
%                        harmx.dBc contains harmonic in dBC
%			 type harmx will give all available results.
%
% e.g. 3: All in e.g. 2, original waveform, fitting waveform and
% error signal assigned to variable
% 
%    [dx harmx] = effbits( data, fin, fs, 1, 7, 1, 1);
%    dx.fit: fitted sine wave
%    dx.orig: original data
%    dx.err: total error
%    To plot fitted sine: plot(dx.fit(:,1), dx.fit(:,2))
%
% Author Bill Tsang (billtsang@gmail.com)
% function [ d, outHarm ] = effbits( data, fin, fs, modulo, Nh, isplot, detail)
% [ d, outHarm ] = effbits( data, fin ) ;
% [ d, outHarm ] = effbits( data, fin, fs ) ;
% [ d, outHarm ] = effbits( data, fin, fs, 1 ); % plot mod 1/fin output
'''
import numpy as np
#import sinefitlsq3 as lsq
from modx import modx
from numpy import log10,sqrt
from matplotlib.pyplot import savefig,plot,show,grid,title,figure,xlabel,ylabel,subplot,legend
from datetime import datetime
pi = np.pi
round = np.around
max = np.amax

'''
% np.sine wave fitting from noisy np.sine signal 
% phase fitting has to be considered 
% with a fixed omega! 
% DESCRIPTIONS:
% s0: sampled series. 1xNp (note here) 
% omega: known freq. (2*pi*f) (rad/sec.) 
% Ts: sampling period (in Sec.) 
% t: time sequence correspond to the data; can be non-uniform
% Ahat: estimated amplitude 
% Theta: fitted theta_0 (in rad.) 
% RMS: root mean squares. 
% os (dc) is also fitted
% modified to accomodate non-unifotm
'''
import matplotlib.pyplot as plt
def sinefitlsq3(s0=np.array([1,2,3,4]),omega=2,t=np.array([0,1,2,3]),Ts=1,DEBUG=0):
	Np=len(s0); # length of data
	'''	
	[r, c] = s0.shape()
	#t=t0+[0:Np(2)-1]*Ts; # removed, becasue t is from input argument
	if r < 2:
		print('sinefitlsq3: input data must be a column vector');
	end
	Nt = len(t);
	if Nt <= 1:
		print('sinefitlsq3: time must be a row vector');
	end
	'''
	
	A11= np.dot(np.sin(omega*t),np.sin(omega*t)); #  sum(sin^2wt)
	A12= np.dot(np.sin(omega*t),np.cos(omega*t)); #  sum(sin * cos)
	A22= np.dot(np.cos(omega*t),np.cos(omega*t)); #  sum(cos^2wt)
	A33= len(s0);
	A13= np.sum(np.sin(omega*t));
	A23= np.sum(np.cos(omega*t));
 
	b1=np.dot(s0,np.sin(omega*t)); # dot product
	b2=np.dot(s0,np.cos(omega*t)); # dot product
	b3=s0.sum();	# total sum

	A=np.array([[A11,A12,A13],[A12,A22,A23],[A13,A23,A33]]);
	Ainv=np.linalg.inv(A);
	Alpha=np.dot(np.linalg.inv(A),np.array([[b1],[b2],[b3]])); 
	#print(A)
	#print(Ainv)
	#print(Alpha)
# be careful here... 
	os = Alpha[2];
	Asintheta=Alpha[1];Acostheta=Alpha[0]; 
	#print(len(os))
	Ahat=np.sqrt(Asintheta*Asintheta+Acostheta*Acostheta); 
	Theta=np.arctan2(Asintheta,Acostheta); 
	RMSerr=np.sqrt(np.dot((s0-Ahat*np.sin(omega*t+Theta)),(s0-Ahat*np.sin(omega*t+Theta)))/Np); 
	if DEBUG:
		plt.figure();	
		plot(t,s0,'k:',t, Ahat*np.sin(omega*t+Theta),'-r'); 
		legend(['measured (RMS=' +str(RMS), 'f=' + str(omega/2/np.pi)]) 
		print('##sinefitlsq3')
		print('os='+str(os))
		print('Amp='+str(Ahat))
		print('phase(Deg)='+str(Theta/np.pi*180))
		print('RMS='+str(RMSerr))
	return(Ahat,Theta,RMSerr,os) 

def rms(x):
    return np.sqrt(x.dot(x)/x.size)
def	effbits(data, fin, fs, modulo=1, Nh=7, doPlot=1, detail=0, DEBUG=0):
	fit_rnd = 1; 
	t0 = 0;
	lw=1;
    #parsing input data
	[sa,sb] = data.shape;
	if sb == 1:
		code=data; 
		code_name='orig data';
		t0 = 0;
		if fs == "":
			print('No time nor fs given')
		else:
			time = np.linspace(t0,(length(code)-1)/float(fs)+t0,len(code));
	elif sb >= 2: # for two column data
		code = data[:,1]
		code_name='orig data';
		time = data[:,0];
		t0 = data[0,0];
		fs = 1/(data[2,0]-data[1,0]); # get fs from column 1, which is index 0 in python
	xmax = fin;
	fit_lng = sa;
# end of parsing input data 
#%%%%%%%%%%%%%%%%%%%%%%%%%%
# Define class for output parameters
	class enob:
		'''Paramters related to fundamental frequency'''
		t0=0
		Nsamp=1
		fin=0
		fs=0
		sndr=0
		snr=0
		enob=0
		dc=0
		phase=0
		phaseDeg=0
		amp=0
		rms=0
		thd=0
		thdRMS=0
		totalErrorRMS=0
		delayPs=0
	d = enob
#d = struct('sndr',0,'snr',0,'enob','0','dc',0,'phase',0, ...
#    'phaseDeg',0,'amp',0,`:0'rms',0,'thd',0);
#% harm[1,1:2]=fundamental ampl, dBC,  phase, phaseD,
#% harm[2,1:2]=2nd order ampl, dBc, phase, phaseD,
	harm = np.zeros([Nh,8]);
	d.rms = rms(code) ;
	A = np.zeros([Nh,1]);
	phase = np.zeros(Nh);
	RMS = np.zeros([Nh,1]);
#%%%%%%%%%%%%%%%%%%%%%%%%%%
#% Find os, fundamental and harmonics
#%%%%%%%%%%%%%%%%%%%%%%%%%%
#% find DC terms
	for j in range(1,(fit_rnd+1)):
#cds is column vector;
		start0 = (j-1)*(np.floor(sa/fit_rnd))
		cds = code[start0:start0+fit_lng];
		fitted_harm = np.zeros([fit_lng,Nh])
		HD = 1 ; 
# t and time are the sample times
		t = time;
# sinefitlsq assume row vector input data and t vector
# % [ A(HD), phase(HD), RMS(HD) ] = sinefitlsq(cds'-osin,2*pi*fin*HD,t0,1/fs);
# also fit offset, more accurate if not complete cycle and fewer data
# pts, non-uniform sampling
		#[ A[HD-1], phase[HD-1], RMS[HD-1], osin] = lsq.sinefitlsq3(cds,2*pi*fin*HD,t);
		[ A[HD-1], phase[HD-1], RMS[HD-1], osin] = sinefitlsq3(cds,2*pi*fin*HD,t);
		Ain = A[1-1];
		curve_fit = osin + Ain*np.sin(2*np.pi*fin*t+phase[0]);
# remove the fundamental and do harmonics fit gives better results.
		fitted_signal = curve_fit;
		for HD in range(2,Nh+1):
			#print(HD)
			residue_signal = cds - fitted_signal;
			#print(residue_signal)
			[ A[HD-1], phase[HD-1], RMS[HD-1], junk] = sinefitlsq3(residue_signal,2*pi*fin*HD,t);
			fitted_harm[:,HD-1] = A[HD-1]*np.sin(2*pi*fin*HD*t+phase[HD-1]);
			fitted_signal = fitted_signal + fitted_harm[:,HD-1];

#%%%%%%%%%%%%%%%%%%%%%%%%%%
#% reconstruct fundamental from fitting A, phase
	err_fit = cds - curve_fit;
	pwr_err = rms(err_fit)**2; 
	pwr_thd = 0; 
	HDy=np.zeros([Nh,fit_lng])
	pwr_HD=np.zeros([Nh,1])
	for HD in range(1,Nh+1): # fundation is index 0 in array
		HDy[HD-1,:] = A[HD-1]*np.sin(2*pi*HD*fin*t + phase[HD-1]) ;
		pwr_HD[HD-1] = (rms(HDy[HD-1,:]))**2;
		harm[HD-1,1] = A[HD-1];# ampl of fundamental
		harm[HD-1,2] = 20*np.log10(rms(HDy[HD-1,:])/np.sqrt(pwr_HD[1-1])); # HD in dB
		harm[HD-1,3] = phase[HD-1]; # phase
    #% phase in deg
		harm[HD-1,4] = phase[HD-1]/pi*180; # phase in degree
    #% relative phase in Deg
		harm[HD-1,5] = ( phase[HD-1]*HD-phase[1] )/pi*180/HD; 
    #% delay in dec
		harm[HD-1,6] =  ( harm[HD-1,4] ) / 360 * 1/(fin*HD);
	# rms value of the harmonics
		harm[HD-1,7] = A[HD-1]/np.sqrt(2); 
		if HD > 1:
			pwr_thd = (rms(HDy[HD-1,:]))**2 + pwr_thd;
	SNDR_fit = 10*np.log10(pwr_HD[1-1]/pwr_err);
	ENOB_fit = (SNDR_fit-1.76)/6.02;
	if 0:
		print('SNDR= ' + str(SNDR_fit[0]))

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
#% assign outputs
##################################
	yDist =  np.zeros(fit_lng);# raw vector
	#print rms(yDist)
	#print(yDist.shape, HDy.shape);
	for HD in range(2,Nh+1):
		yDist = yDist + HDy[HD-1,:];
	timeRow=time.reshape(1,fit_lng);
	# column array
	Dist = np.transpose(np.array([time, yDist]));
	#print type(timeRow)
	#print timeRow.shape,time.shape,yDist.shape
	#print('Dist',Dist.shape)
	yNoise_jitter = err_fit - yDist ; # jitter + noise
	#print time
	#print(Dist[:,0])
	#print timeRow.shape,rms(yDist),type(time),type(yDist)
	SNR = 20*np.log10(np.sqrt(pwr_HD[1-1])/rms(yNoise_jitter));
	sigRMS = np.sqrt(pwr_HD[1-1]);
	totalnoiseRMS = rms(yNoise_jitter) ;
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#% Looking for jitter
#% Note: After modx time is unequally spaced
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if sa < 6:
		print('Too few samples for fitting jitter')
		jitter = 0;
	else:
		yNoise_jitter_time=np.transpose(np.array([time,yNoise_jitter]));
		#print(yNoise_jitter_time.shape);
		noise_jitter = modx(yNoise_jitter_time,fin,DEBUG=0);
		#print(noise_jitter)
    #% calculating phase of each sample from 0 to 2pi
    #%tphase = modx([time, mod(time,1/fin)/(1/fin)*2*pi], fin) ;
    #%tphase = mod(time, 1/fin) ; pause
    #%d.tphase = modx([time, mod(mod(time,1/fin)/(1/fin)*2*pi+phase(1),2*pi)], fin) ;
    #%noise_jitterVSphase = [tphase(:,1), noise_jitter(:,2)] ;
		if 0:
			figure;
			#plot(noise_jitter(:,1), noise_jitter(:,2));axis tight; hold on;
			#xlabel('time [sec]');
			#ylabel('total noise');
			#%plot(tphase(:,2), noise_jitter(:,2).^2,'r');
			#%noise_sort = sortrows(d.noise_jitter_vs_phase,1);
			#%hold on;
			#%plot(noise_sort(:,1), noise_sort(:,2))
		x45cross = pi/4 - phase[1-1];
		#print(phase[1-1],x45cross,'test');
		x135cross = 3*pi/4 - phase[1-1];
		x225cross = np.mod(5*pi/4 - phase[1-1],2*pi);
		x315cross = np.mod(7*pi/4 - phase[1-1],2*pi);
    # For convenience, make sure 0 =< x45cross < pi
		if x45cross < 0:
			x45cross = pi + x45cross;
		elif x45cross >= pi:
			x45cross = x45cross - pi;
		x135cross = pi/2  + x45cross;
		x225cross = pi + x45cross; 
		x315cross = np.mod(3*pi/2 + x45cross,2*pi); 
    # 45 degree phase, convert phase to bin #
		#print('test', max([round(x45cross/2/pi*fit_lng),1]));
		x45crossn = max([round(x45cross/2/pi*fit_lng),1]);
		x135crossn = round(x135cross/2/pi*fit_lng);
		x225crossn = round(x225cross/2/pi*fit_lng);
		x315crossn = max([round(x315cross/2/pi*fit_lng),1]);
	# tbin should be 1/4 of period
		binW = np.floor(fit_lng/4);
		Nbin45 = x135crossn - x45crossn;
		Nbin135 = x225crossn - x135crossn;
		if x315crossn > x225crossn:
			Nbin225 = x315crossn - x225crossn;
		else:
			Nbin225 = fit_lng - x225crossn + x315crossn;

		Nbin315 = fit_lng - Nbin45 - Nbin135 - Nbin225;
		if DEBUG:
			print("#45deg: %d, 135deg: %d, 225deg: %d, 315deg: %d\n" %(x45crossn, x135crossn, x225crossn, x315crossn));
		x45crossn;
		x135crossn;
    # integrate noise from phase 45 to 135 (nlow)
    # integrate noise from phase 135 to 225 (nhigh)
    # nhigh - nlow = (jitter*Ain*2*pi*fin)^2
		noise_low = sum(noise_jitter[x45crossn:x135crossn-1,1]**2)/Nbin45;
		noise_high = sum(noise_jitter[x135crossn:x225crossn-1,1]**2)/Nbin135;
		if x45cross >= pi/2: # x315 is >> 2pi, wrapped around
			#x315crossn could be at 1 bin
			noise_low2 = (sum(noise_jitter[x225crossn:len(cds),1]**2) + \
			sum(noise_jitter[0:max(x315crossn-1,0),1]**2) )/Nbin225;
			noise_high2 = sum(noise_jitter[x315crossn:max(x45crossn-1,0),1]**2)/Nbin315;
		elif x45cross >= 0: # x315 <= 2pi
			#x45crossn could be at 1 bin
			noise_low2 = sum(noise_jitter[x225crossn:x315crossn-1,1]**2)/Nbin225;
			noise_high2 = ( sum(noise_jitter[x315crossn:len(cds),1]**2) + \
			sum(noise_jitter[1:max([x45crossn-1,1]),1]**2) )/Nbin315;
		if 0:
			t_45cross 
			t_135cross 
			t_225cross
			t_315cross
			Nbin45 
			Nbin135
			Nbin225
			Nbin315
			disp(sprintf( 'noise low, high : %.4g %.4g %.4g %.4g',\
			 noise_low, noise_high,noise_low2,noise_high2));

		if noise_high+noise_high2 < noise_low+noise_low2:
			jitter = 0;
		else:
        # noise due to jitter
        # vn^2 = (jitter * Ain * 2 * pi * fin * np.cos( 2*pi*t ))^2
        # average noise from jitter = 1/2*(jitter * Ain * 2 * pi * np.cos( 2*pi*t ))^2
        #jnoise_delta = (noise_high - noise_low);
	# find noise average over the full cycle
			jnoise_delta = (noise_high + noise_high2 - noise_low - noise_low2)/2;
			jitter = np.sqrt(jnoise_delta*(pi/2)/(2*pi*Ain*fin)**2);
		jnoiseRMS = np.sqrt((jitter * Ain * 2 * fin * pi)**2/2);

		if  ( rms(yNoise_jitter)**2 > jnoiseRMS**2 ):
			thermNoiseRMS = np.sqrt(rms(yNoise_jitter)**2 - jnoiseRMS**2);
		else:
			thermNoiseRMS = 0;
#	# Assign output
		d.tnoiseRMS = totalnoiseRMS;
		d.jnoiseRMS = jnoiseRMS;
		d.thermnoiseRMS = thermNoiseRMS;
		d.jitter_sec = jitter ;
		if DEBUG:
#			%x90cross = pi/2 - phase(1);
#			%x180cross = pi - phase(1);
#			%Nbin2 = x90crossn - x45crossn;
#			%x90crossn = round(x90cross/2/pi * length(cds))
#			%x180crossn = round(x180cross/2/pi * length(cds))
			totalnoise_rms = sqrt(sum(noise_jitter[:,1]**2)/len(cds));
			jnoise_cal = sqrt((1e-12*2*pi*fin*Ain)**2/2);
#			%noise_low2 = sum(d.noise_jitter(x45crossn:x90crossn-1,2).^2)/Nbin2
#			%noise_high2 = sum(d.noise_jitter(x135crossn:x180crossn-1,2).^2)/Nbin2
#			%noiseJ2 = (noise_high2 - noise_low2)
#			%jitter2 = sqrt(noiseJ2/(2*pi*Ain*fin)^2)     

#Assign output variable.
	if 1:
		d.dc = osin;
		d.amp = Ain;
		d.fin = fin;
		d.fs =  fs;
		d.phase = phase[1-1];
		d.phaseDeg = d.phase/2/pi*360;
		d.delayPs = harm[1-1,6]*1e12;
		d.t0 = t0;
		d.sndr = SNDR_fit;
		d.snr = SNR;
		d.enob = ENOB_fit;
		d.thd = 20*log10(np.sqrt(pwr_thd)/np.sqrt(pwr_HD[1-1]));
		d.thdRMS = np.sqrt(pwr_thd);
		d.totalErrorRMS = np.sqrt(pwr_err);
		d.Nsamp = sa ; 
		d_orig = np.transpose(np.array([time, cds]));
		d_fit = np.transpose(np.array([time, curve_fit]));
		d_err = np.transpose(np.array([time, err_fit]));
	if 1:
		d.fit = d_fit;
		d.orig = d_orig;
		d.err = d_err;
	class HarmClass:
		''' Parameters related to harmonic distortion '''
		HD=0
		Amp=0
		dBc=0
		phaseRad=0
	outHarm=HarmClass
	outHarm.HD = range(1,Nh+1) ;
	outHarm.Amp = harm[:,1];
	outHarm.dBc = harm[:,2];
	outHarm.phaseRad = harm[:,3];
	outHarm.phaseDeg = harm[:,4];
	outHarm.phaseRel = harm[:,5];
	outHarm.Delay_ps = harm[:,6]*1e12;
	outHarm.RMS = harm[:,7];
	#% checking if result make sense
	if SNR < SNDR_fit:
		print('warning:SNR is greater than SNDR')
	if jitter == 0:
		thermNoiseRMS = totalnoiseRMS;
		jnoiseRMS = 0;
		print('warning: Noise at peak of sine wave is higher than noise at the highest slope')

# disp result to screen
	print("os=%.4g\nAin=%.4g\nArms=%.4g\nfin=%.10g\nSNR(dB)=%.4g\nSNDR(dB)=%.4g\nENOB=%.4g\nTHD(dB)=%.4g\n" %(osin, Ain, sigRMS, fin,SNR, SNDR_fit,ENOB_fit,d.thd));
	print('offset ampl(pk), sigRMS, fin, SNR(dB), SNDR(dB), ENOB_fit, THD(dB)')
	print("%.4g %.4g %.4g %.10g %.4g %.4g %.4g %.4g\n" %(osin, Ain, sigRMS, fin,SNR, SNDR_fit,ENOB_fit,d.thd));
	print('THDrms, jitter(ps,rms), jitter noise(rms), total noise(rms), thermal noise(rms)')
	print("%.4g %.4g %.4g %.4g %.4g\n" %(np.sqrt(pwr_thd),jitter,jnoiseRMS,totalnoiseRMS,thermNoiseRMS));
	print('Fundamental: A*sin(2*pi*fin*t + phase)')
	print("HD \t Amp \t dBc \t phase(Deg) \t Delay(ps)"); 
	for HDx in range(0,Nh):
		print("#%d %0.6f \t %0.3f \t %0.8g \t %0.8g " %(HDx+1,harm[HDx,1],\
	harm[HDx,2],harm[HDx,4], harm[HDx,6]*1e12)); 
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
#% plotting the fitted waveform
	OUTBASE="%s" %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S-%f'));
	TF=10; # title fontsize
	#print 'test',doPlot,modulo
	if (doPlot and modulo == 0 or False ):
		figure;
		subplot(2,1,1)
		plot(t,err_fit,'b',linewidth=2,hold=1); grid;
		plot(Dist[:,0],Dist[:,1],'r',linewidth=2); 
		#s = sprintf('SNDR = %.4gdB, ENOB = %.3g',SNDR_fit,ENOB_fit);
		s = "SNDR = %.4gdB,SNR = %.4gdB, ENOB = %.3g, Fin = %.3g, Fs = %.3g, THD = %.3gdB, Jitter = %.3gs" \
		%(SNDR_fit,SNR,ENOB_fit,fin,fs,d.thd,jitter);
		s = s+'\n';
		#%%% set legend font
		legend(["total err", "harmonics"]); 
		grid
		#ch = get(h,'Children');
		#set(ch(3),'FontSize',10);
		title(s,fontsize=TF); 
		ylabel('Error, Harmonics');
		subplot(2,1,2)
		plot(t,cds,'r',linewidth=lw );
		plot(t,curve_fit,'k',linewidth=lw,hold=1);
		h=legend(['%s'%(code_name), 'fitted'])
		grid;
		#ch = get(h,'Children');
		#set(ch(3),'FontSize',10);
		ylabel('Mag')
		xlabel('Time[sec]')
		#axis([0 max(t) min(code)-10 max(code)+10]);
		#title(s,'FontSize',TF);   hold off;
		title("%d samples" %(fit_lng),fontsize=TF); 
	elif ( doPlot and modulo ):
		figure;
		H=subplot(2,1,1);
		#%set(H, 'FontSize', 12, 'FontWeight', 'bold');		
		d_err_mod = modx(d_err,fin);
		Dist_mod = modx(Dist,fin);
		plot(d_err_mod[:,0],d_err_mod[:,1],'b',linewidth=2); grid;
		plot(Dist_mod[:,0],Dist_mod[:,1],'r',linewidth=2);
		s = "SNDR = %.4gdB,SNR = %.4gdB, ENOB = %.3g, Fin = %.3g, Fs = %.3g, THD = %.3gdB, Jitter = %.3gs" \
		%(SNDR_fit,SNR,ENOB_fit,fin,fs,d.thd,jitter);
		s = s+'\n';
		title(s,fontsize=TF); ylabel('Error, Harmonics');
		h=legend(['total err', 'harmonics'],loc=0);grid ;
		#ch = get(h,'Children');
		#set(ch(3),'FontSize',10);
		#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		H=subplot(2,1,2);
		#%set(H, 'FontSize', 12, 'FontWeight', 'bold');
		d_orig_mod = modx(d_orig,fin);
		d_fit_mod = modx(d_fit,fin);
		plot(d_orig_mod[:,0],d_orig_mod[:,1],'k',linewidth=lw);
		plot(d_fit_mod[:,0],d_fit_mod[:,1],'r',linewidth=lw );
		h=legend(['%s'%(code_name), 'fitted'],loc=0)
		grid
		#ch = get(h,'Children');
		#set(ch(3),'FontSize',10);
		ylabel('Mag')
		xlabel('time [s]')
		title('%d samples' %(len(cds))+',up to %d-th harmonics' %(Nh),fontsize=TF); 
	savefig('eb'+OUTBASE,dpi=600)
	show()
	return(d,outHarm)
def main(args):
	import twoCol2Array as tca
	data=tca.twoCol2Array(args.file)
	#print(type(data))
	#print(args.DEBUG)
	effbits(data,fin=args.fin,fs=args.Fsamp,modulo=args.modulo,Nh=args.Harm,detail=args.detail,doPlot=args.doPlot,DEBUG=args.DEBUG)

if __name__ == "__main__":
	import argparse
	import sys
	parser = argparse.ArgumentParser(description="Find enob using curve fitting. Input could be two column or on column data", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	# with optional argument ?
	parser.add_argument('-f','--fin', type=float,default=1, required=True, help='Signal Frequency')
	parser.add_argument('-F','--Fsamp', type=float, default=10,help='Sampling Frequency')
	parser.add_argument('-H','--Harm', type=int, default=7,help='h-th harmonics to label')
	parser.add_argument('-p','--doPlot', action='store_true', default=True, help='Show fft plot')
	parser.add_argument('-d','--detail', action='store_true', default=True, help='Show detail output')
	parser.add_argument('-D','--DEBUG', action='store_true', default=False, help='Show DEBUG output')
	parser.add_argument('-m','--modulo', action='store_true', default=True, help='Show modulo of the sinewave. Only one cycle is shown')
	parser.add_argument("file", type=str, help="Input data file")
	#parser.print_help()
	args = parser.parse_args()
	main(args)

