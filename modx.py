def modx(code, fin, fs=0, DEBUG=0):
	'''
	% function modx(code, fin, fs)
	% Return the data with modulus in time.
	% code	:	numpy array, one column or two columns
	% Time is mod by 1/fin
	% use scatter to plot the outputs
	% scatter(a(:,1),a(:,2),0.1)
	'''
# check if the input is single column, two-columns (time, data)
	import numpy as np
	import matplotlib.pyplot as plt
	[sa,sb] = code.shape
	#print('Input must be a vector')
	if sb == 1:
		N = len(code)
		x = np.linspace(0, (N-1)/fs, N); # x must be a vector
		y = code[:,1];
	elif ( sb >= 2 ):
		x = code[:,0];
		y = code[:,1];
	elif sb == 1 and fs == "":
		print('fs must be provide since time reference is not in input data') 
	else:
		print('Check input')
	T=1/float(fin)	
	Tarr=1/float(fin)*np.ones([1,sa]);
	xmod=np.mod(x,Tarr);
	xmod.shape=(sa,1)
	#print xmod.shape,y.shape
	#print type(xmod),type(y)
	codeMod = np.transpose([xmod,y]);
	output = codeMod[codeMod[:,0].argsort()]
	if DEBUG==1:
		print(output.shape)
		#print(x)
		#print(y)
		#print(output)
		#print(codeMod)
		plt.plot(output[:,0],output[:,1],'-*')
		plt.xlabel('Time[s]')
		plt.ylabel('Ampl[v]')
		plt.grid()
		plt.show()
	np.savetxt('temp.ap',output)
	return output

