#!/usr/bin/python

# -*- coding: utf-8 -*-
# plot two column as x-y pair. Lines don't start with number is ignored. 
# it can choose to plot multiple columns, 9 columns max
# eg: plotxy('data.txt',1,2,3)
#	plot col 2 and 3 against 1
# Need to import the plotting package:
# import plotxy
# plotxy.plotxy('FILENAME')
#def plotxy(INFILE, [1,2[,3]], 
BLOCK=1
VERBOSE=0
TITLE='NoTitle'
XLABEL='time'
YLABEL='Volt'
from datetime import datetime
OUTFILE='plot%s' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S-%f'))
OUTFILE='temp.png'
import sys
def plotxy(INFILE, args):
    import matplotlib.pyplot as plt
    import numpy as np
    import re
    print "plotxy " + str(INFILE)
    nrow=0
	#for COL in *argv:
    # Read the file. 
    f2 = open(INFILE, 'r')
    # read the whole file into a single variable, which is a list of every row of the file.
    lines = f2.readlines()
    f2.close()
    # column 1
    i=0
    col=[]
    for colx in args:
		#print 'test2',args[0]
		col.append(int(colx))
    # initialize some variable to be lists:
    NCol = len(col)
    x1 = []
    y2d=[[] for i in xrange(8)]
    print('column' + str(col))
    # scan the rows of the file stored in lines, and put the values into some variables:
    for line in lines:
        #remove leading and trailing whitespce
        linetmp = line.strip()
        if VERBOSE == 1:
            print(linetmp)
        # filter out comment start #
        if not linetmp.startswith("#"):
            # if first column is a number
            match=re.search('^[-0-9.]',linetmp)
            matchWhite=re.search('\s',linetmp)
			# 8 row list
            #print(y2d)
            if match:           
                p = linetmp.split()
                if nrow == 0:
					N = len(p)
                #print('ncolumn ', p)
                for cx1 in col:
					if cx1 > N:
						print("error: Only %d column in file but column %d is specified." % (N,cx1))
                x1.append(float(p[col[0]-1]))
				# c2 is out of range, set c2=c1
                #if col[1] >= N:
				#	col[1] = col[0]
                #print x1,float(p[col[0]-1])
			
                for j in range(1,NCol):
					#print(j,col[j],N)
					y2d[j-1].append(float(p[col[j]-1]))
					#print(y2d)
                nrow=nrow+1 
                #print('nrow' + str(nrow)) 
            elif matchWhite:
			    print('Non-number line found in file')
			    print(linetmp)
			    matchWhite=False #  reset     
            else:
				print('Data format not supported, data is neither number, empty lines, comments')
    if NCol == 1:
    	y2d = x1
    	x1 = range(nrow)
    y1=y2d
    xv = np.asarray(x1)
    yv = np.asarray(y1)
    #print xv,yv
    if NCol > 1:
		for i in range(NCol-1):
#print(xv,yv) 
#now, plot the data:
			#print 'test', i
			ytemp=yv[i]
			plt.plot(xv,ytemp)
    else:
		plt.plot(xv,yv)
    plt.hold(True)
    plt.xlabel(XLABEL)
    plt.ylabel(YLABEL)
    plt.title(TITLE)
    plt.legend(args[1:])
    plt.grid()
    plt.savefig(OUTFILE)
    if BLOCK == 1:
	    plt.show()
    else:
		plt.show(block=False)
def main():
	#print 'main function'
	INFILE=sys.argv[1]
	#M=len(sys.argv)
	#for i in range(2,M):
	#	print sys.argv[i],i,M
	#print('input file name: '+INFILE)
	#plotxy(INFILE,sys.argv[2],sys.argv[3])
	plotxy(INFILE,sys.argv[2:])

if __name__ == "__main__":
	main()




